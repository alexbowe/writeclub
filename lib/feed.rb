require 'rss'
require 'open-uri'

module Writeclub

  class Feed
    
    def self.all
      [
        Feed.new('alexbowe', 'http://www.alexbowe.com/rss.xml'),
        Feed.new('google', 'http://googleblog.blogspot.com.au/atom.xml')
      ]
    end
    
    attr_reader :name, :address
    
    def initialize name, address
      @name, @address = name, address
    end
    
    def read
      content = ""

      open address do |f|
         content = f.read
      end

      RSS::Parser.parse content, false
    end
    
  end
  
end