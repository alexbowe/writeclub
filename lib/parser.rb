class Writeclub::Parser
  
  def self.last_posted feed
    posts = feed.read.items
    
    posts.first.published # pubDate
  end
  
end