require 'active_support'
require_relative '../writeclub'
Writeclub::Feed.all.each do |feed|
  posted = Writeclub::Parser.last_posted feed
  now = Time.now
  if posted.between? (now-60*60*24*14), now
    puts "Good job, #{feed.name}"
  else
    puts "Bad job, #{feed.name}"
  end
end